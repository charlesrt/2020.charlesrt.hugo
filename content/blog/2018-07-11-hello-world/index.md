---
title: "Hello World"
date: 2018-07-11T08:24:18+01:00
url: "blog/hello-world"
keyword: "hello"
tags: [redesign, dat, beaker]
---

{{< figure src="screenshot-2018-07-11-08-50-04.png" caption="A screenshot of my zero dawn refresh served over the peer-to-peer web via Beaker Browser. It works!" >}}

I have been meaning to redesign my website for a long time. I want to do more than just another _re:skin_ so I am starting from scratch and designing the site live; partly to open my process to others who want to see and learn, partly to hold myself accountable so I do it properly.

Live design: means more than just having my code commits in the open on [Gitlab](https://gitlab.com/charlesrt/charlesrt.hugo). Instead of large wholesale design changes magically appearing on the internet, each individual change I make can be tracked live over **dat://** the peer-to-peer web, enabled by the [Dat Protocol](https://datproject.org). If you use [BeakerBrowser](https://beakerbrowser.com) you can turn on _live reloading_, to experience changes and blog updates auto refresh, as they happen. This, essentially, allows me to create a live blogging engine without any additional code.

You will still be able to refresh [the old fashioned way] over **https://** if you use any other browser.

I can't promise I will blog about the reason behind every design decision: but I will try.

This, is all part of my commitment to support a better, open and decentralised web for the future.
