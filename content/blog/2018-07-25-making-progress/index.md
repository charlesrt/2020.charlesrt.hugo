---
title: "Making progress"
date: 2018-07-25T10:23:59+01:00
description: "I'm mostly relying on train journeys to make updates so progress has been slow but I'm getting somewhere."
url: "blog/making-progress"
keyword: "progress"
tags: [redesign]
---

{{< figure src="screenshot-2018-07-25-10-23-21.png" caption="Things are still looking basic but I'm making progress" >}}

I've had some time away and I'm mostly relying on train journeys to make updates so progress has been slow but I'm getting somewhere.

All of the important content from my previous website build has been migrated and is better structured from a directory perspective. Redirects are in place to ensure I don't break the web and I have a [Gulp](https://gulpjs.com) task running to compile [Stylus](http://stylus-lang.com) (my preprocessor of choice) into CSS.

I'm ready to think about my [design goals](/blog/design-goals) and how I will structure the content better for the user so that navigation is contextual and I'm getting across the right message and flow.

Before that, I'm going to consider what a good baseline CSS should be.
