---
title: "Be safe; be human"
date: 2020-03-20T15:26:37Z
description: "Principles for sane working in insane times. CEOs, directors, leaders, managers, etc, need to put the self-created work-machine to one side and remember that everyone they work with is a human being."
url: "blog/be-safe-be-human"
tags: [principles, leadership, isolation]
images:
- /blog/be-safe-be-human/be-safe-be-human-principles.png

---

## Principles for sane working in insane times

After a full week working from home due to the (COVID-19) coronavirus pandemic, I have observed great differences in human behaviour. Some caring. Some seriously lacking in the human department. It's clear that with uncertain times, comes uncertain behaviour. CEOs, directors, leaders, managers, etc, need to put the self-created work-machine to one side and remember that everyone they work with is a human being.

{{< figure src="be-safe-be-human-principles.png" caption="8 Principles for sane working in insane times" >}}

I'm responsible for a team of 16 people who are all working remotely now, from home. Putting the challenges of technology aside--like getting video conferencing to work consistently--many are struggling. It's not like they haven't worked from home before. But they've never had to do it for an entire week, not knowing how many weeks they'll have to keep going for while the world around them is becoming a scarier and scarier place.

When we're all in the office, we're on common ground. Everyone's environment is the same, we've got oversight on what's happening around us and we don't get that fear of missing out. In isolation, we do share some common ground in that we're all remote. It's clear, communication works better when everyone is remote, compared to a few people remotely joining a co-located team. But, one thing that's not common anymore is the environment each individual is in.

Some people live in houses with a garden, others in studio apartments. Some already have their children home with them, some don't have children at all. Some are sharing their home-work space with their partners, others have got access to separate quiet spaces. Each individual's working environment is now different, so you need to be accommodating for a variety of needs and set standards that support the many.

## Looking after your people is your job

As a CEO, or director, or leader, or manager, or someone who is responsible for a team of people, it's your job to look after them. The worse thing you can do, is assume everyone's environment and circumstances are the same as yours or impose yours on others. I have said a lot to my team this past week to reassure them and intentionally put time in place for the community to come together. The following principles should to be considered by everyone responsible for a team to reassure ways of working.

### 1. Everyone is still working, just remotely

Your team shouldn't feel like they have to compensate for working from home--it's still work. No-one has chosen to work from home for weeks while everyone else is in the office. Everyone needs to adapt and you need to accept that things are different now.

### 2. Forget the 9-5

I don't care how many hours my team work right now. The normal 9-5 office hours are imperfect anyway. How many actual hours of productive work do you think people do in the office once you account for all the distractions, context switching and impromptu meetings? "Have you got five minutes for a quick chat", almost always turns into an hour long conversation. If your team are delivering outcomes and bringing their whole self when they are working, it doesn't matter if they only work four hours one day.

### 3. Children are an unexpected reality

I've heard of people being docked pay by their employer because they had their children at home this week. The majority of people are going to have their children at home with them from now on. It doesn't mean they're not working. Parents working at home with children are dealing with a context-switching-hurricane work environment. It's volatile; they're trying to entertain and educate their children and do their paid job.

You need to allow parents greater flexibility to deal with the unexpected reality of caring for a small human. This flexibility doesn't need to stop with parents. You should apply it to your whole team. If someone needs to stop work and get out of the house for a few hours, in the garden or to the park, they need to feel they have permission to do so. You can give them that reassurance and reassure yourself: the work will happen, just later.

### 4. Partners are new teammates

Most of the time people would never choose to work with their partner. But a lot of people are now stuck sharing workspace with their partner. They've got a job to do too. If they're in the background of a video meeting or need to take a call at the same time: it's okay. It's no different to the background noise and people when you're in the office.

### 5. It's okay to slow down

Work shouldn't have to be crazy at the best of times so it certainly doesn't have to be crazy now. There is a lot going on in the world, when suddenly, all of the news feels relevant. You can reassure people--they don't need permission to take they're foot off the accelerator and slow down. If someone needs to take a break and come back to something later: do.

### 6. Be open about your own struggles

People tend to follow authority. As a leader or manager, that means you. The way you work will dictate how your team thinks they should be working. So be open about your own struggles. It's okay to show weakness and it might actually strengthen your team's view of you.

### 7. Make time for communities

Outside of project work and meetings it's good for your team and the community to make time to get together (remotely). This can be centred around a specific topic or could just be set time for random chat. These side-conversations used to happen naturally in the office whereas now, they might need to be forced a little. Create sessions for a daily coffee chat or a weekly social. Let people know, it's not mandatory, it's not another presence prison. If no-one attends the end of week social because they'd rather use that time to be with their families sooner: that's okay (I know, I'd prefer that).

### 8. Be safe; be human

None of this is extraordinary, it's just simple human touches. In times like this, it should be clearer than ever: work is not the most important thing in the world. I hope everyone is looking after themself and their's. Be safe; be human.

Share the human things you're doing with your teams: tweet me [@charles_rt](https://twitter.com/charles_rt/)
