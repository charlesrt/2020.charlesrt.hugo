---
title: "Design goals"
date: 2018-07-11T09:40:39+01:00
url: "blog/design-goals"
keyword: "goals"
tags: [redesign, goals]
---

Design decisions will be based on a combination of the assumptions I make on what people might want, and the message and flow I want to get across.

I also want to achieving the following goals:

- move away from Jekyll and build my site with Hugo which I've had my eye on ✅
- use only contextual navigation; no _"I don't know what I want"_ menu
- focus on typography; include emoji (the universal language of the future) at every opportunity 😉
- allow for long and short form posts
- create an owned space for my content to exist _first_ before sharing to centralised social networks.

I'll add to and revisit this list of goals as I go.
