---
title: "White rabbit"
date: 2018-10-25T18:39:02+01:00
url: "blog/white-rabbit"
keyword: "rabbit"
tags: [redesign, self, life]
images:
- /blog/white-rabbit/follow-the-white-rabbit.jpg
---

Lately, I've been following the white rabbit relentlessly in the hope that if I keep going I'll eventually stumble upon some life-changing Trinity.

{{< figure src="follow-the-white-rabbit.jpg" caption="Follow the white rabbit." >}}

This Matrix Metaphor is representative of both the unpaid work I've been doing on the redesign of my own website and the paid work I do for UK government.

I've spent time worrying about why certain things aren't working properly and investing too much in trying to find solutions. I've blindly ignored good advice telling me to stop—the effort required isn't worth the reward—and after a while only a mad person would keep going. I am that mad person. Convinced, no. Obsessed, with following the white rabbit in the hope of achieving some self-gratifying win that might make all the effort feel worthwhile.

It's time to be a quitter.

> A lot of times it's better to be a quitter than a hero...people automatically associate quitting with failure, but sometimes that's exactly what you should do. If you've already spent too much time on something that wasn't worth it, walk away. You can't get that time back. The worst thing you can do now is waste even more time. —[Rework](https://basecamp.com/books/rework), Don't be a hero, Page 118-119

For the work I'm doing on my own website that means quitting the crazy rabbit hole of confusing code and ideas I was falling deeper into. I'd lost sight of the goals I was trying to achieve. I'm rolling back a number of files and will revisit the redesign with a clearer, fresher mind another time.

For the paid work I do that means quitting too. Not quitting my job but quitting getting too involved in certain aspects that, right now, can't be influenced positively.

Next week is half-term. I'm going to take some time off to spend it with my family. I'm also going to reread [Rework](https://basecamp.com/books/rework) that contains advice I've been ignoring.

Reset. Rework. Then, keep going on what matters.
