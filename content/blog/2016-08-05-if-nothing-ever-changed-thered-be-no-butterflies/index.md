---
title: "If nothing ever changed, there’d be no butterflies"
date: 2016-08-05T14:55:00+01:00
url: "blog/if-nothing-ever-changed-thered-be-no-butterflies"
keyword: "change"
tags: [leadership, change, self, life]
images:
- /blog/if-nothing-ever-changed-thered-be-no-butterflies/butterfly-photo.jpg
---

Normally, we don’t pay attention to change — we live busy lives — and the changes taking place around us are not worthy of a second thought.

{{< figure src="butterfly-photo.jpg" caption="A butterfly on a leaf" >}}

Occasionally, we allow our perceptions to determine a particular change as having a higher effect on us. That change could be work related; a project cancelled, close team members leaving, organisational change, starting a new job, or it could be personal; having a baby, or moving home. Sometimes that change is outside of our control, sometimes we’ve instigated it — that isn’t what’s important. The important thing is how we let that change effect us; our mood, our outlook and our work.

We can’t always change change, and most of the time it’s futile to try and stop it. When change happens, we need to adapt and evolve. Don’t focus on the negatives or the known unknown, but — if the opportunity arises — do influence change positively. We can’t regret trying to make something better, we can only regret not trying. 

We should embrace change and the new opportunities it may bring, while reflecting on our past decisions so that we learn for the future.

After all, tomorrow is a new day.
