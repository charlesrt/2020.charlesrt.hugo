---
title: "Three week sub-1:30 half marathon training plan"
date: 2018-09-21T16:03:51+01:00
url: "three-week-sub-130-half-marathon-training-plan"
keyword: "half"
tags: [running, training]
---

<table>
    <thead>
      <tr>
        <th>Week</th>
        <th>Monday</th>
        <th>Tuesday</th>
        <th>Wednesday</th>
        <th>Thursday</th>
        <th>Friday</th>
        <th>Saturday</th>
        <th>Sunday</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th>Week 1</th>
        <td data-label="Monday">Rest</td>
        <td data-label="Tuesday">13.1M Race Pace Test</td>
        <td data-label="Wednesday">Cross Train</td>
        <td data-label="Thursday">6.3M Intervals</td>
        <td data-label="Friday">Rest</td>
        <td data-label="Saturday">Parkrun PB Attempt</td>
        <td data-label="Sunday">13.1M Steady</td>
      </tr>
      <tr>
        <th>Week 2</th>
        <td data-label="Monday">Rest</td>
        <td data-label="Tuesday">45min Farklet</td>
        <td data-label="Wednesday">Cross Train</td>
        <td data-label="Thursday">Rest</td>
        <td data-label="Friday">13.1M Progressive</td>
        <td data-label="Saturday">Parkrun PB Attempt</td>
        <td data-label="Sunday">13.1M Steady</td>
      </tr>
      <tr>
        <th>Week 3</th>
        <td data-label="Monday">Rest</td>
        <td data-label="Tuesday">45min Farklet</td>
        <td data-label="Wednesday">Cross Train</td>
        <td data-label="Thursday">6.3M Progressive</td>
        <td data-label="Friday">Rest</td>
        <td data-label="Saturday">Parkrun Steady w. Strides</td>
        <td data-label="Sunday"><strong>Manchester Half Marathon</strong></td>
      </tr>
    </tbody>
  </table>
