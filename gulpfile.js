var gulp = require('gulp');
var stylus = require('gulp-stylus');

gulp.task('styles', function() {
  gulp.src('src/*.styl')
      .pipe(stylus())
      .pipe(gulp.dest('static/'));
});

gulp.task('default', function() {
  gulp.watch('src/**/*.styl', ['styles']);
});
